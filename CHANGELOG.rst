Changelog
=========


v0.1.6 (2013-11-08)
-------------------------------------------------------------

* [Feature] Added migration command `migrate_django_mailer` which copies LogMessages from django-mailer to Log table
* [Feature] Sends sentry message while deferring when raven configuration can be found
* [Fix] Added deferred field to Admin's Queue filter and field list

v0.1.0 - v0.1.5 (2013-11-07)
-------------------------------------------------------------

* [Feature] Added Slovak locale
* Initial release

